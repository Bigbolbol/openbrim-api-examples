# OpenBrIM API Examples #

This repository hosts a set of example projects demonstrating the use of OpenBrIM APIs. 

## C# Examples ##

**TestApp** is the most comprehensive example demonstrating reading the list of projects, opening projects and accessing low level data. 

**EmbeddedUI** is a simple projects demonstrating how the UI of OpenBrIM app can be integrated into your own app. This includes the 3D rendering, 2D CAD drawings and more. 

**TestLibraryObjectAPI** is an example that allows user to browse through the objects defined in the library. 

**OBrIMCustomObjModelSample** is an example demonstrating the recommended way of writing a third party OpenBrIM based .NET application. By setting up your application classes in a certain way, you are ensuring that you have cloud and real-time collaboration capabilities out of the box. 

## VB.NET ##

**OBrIMTestsVB** is an example project in which we can write some test cases. 

**OBrIMVBSample** is an example application that makes use of the UI controls developed for OpenBrIM platform and exposed by OpenBrIM connector.

## Python ##

**OpenBrIM.py** is a python program interacting with OpenBrIM via ReST Direct Data API. 

**Sensor.py** is a python program that uploads sensor data (big data) to OpenBrIM servers via ReST Direct Data API. 

## VBA ##

**OBrIMReST.bas** is a VBA module containing  number of wrapper calls to OpenBrIM ReST Direct API. This module can be added into VBA macros to quickly create a link between your macro and OpenBrIM servers. 

**OpenBrIM_AccessProjectData.xls** is an Excel macro demonstrating the use of OBrIMReST.bas. 


# OpenBrIM API Introduction #

OpenBrIM is an open, collaborative, cloud-based information platform. It is important for OpenBrIM to be easy to integrate with existing software applications our industry use. OpenBrIM provides APIs at 3 main levels. 

**1- Direct Data API** allows simple HTTP requests to directly interact with OpenBrIM cloud service. Only low level functionality is supported at this level, such as:

* a) authentication 
* b) creating/updating/deleting project data/library-object/workspace data. 
* c) access to big-data 
* d) access to revision history and snapshots

This is the door to OpenBrIM backend allowing you to interact with OpenBrIM at data level. This API can be used on any device and platform that is capable of doing simple HTTP requests. 

Because it is a low level API, many of the available functionalities of OpenBrIM App and OpenBrIM Library are not available. 

The format of the requests to this API and the responses may seem awkward as it does not use standard JSON or XML to exchange data. (this will likely change at a later release) 

Big Data API is part of Direct Data API system. Big Data API allows you to interact with the back-end database directly. It is particularly suitable for large data that is not directly part of the project (probably because it is not possible to keep as part of the project due to its size) but should still be associated with the project. Revision control, real-time collaboration, parametric capabilities and all other high level functionality are all turned off for Big Data  API. It is designed for raw performance to store (for example) structural sensor data. You can see an example for Big Data API on this link:

https://openbrim.org/objid6ir17ihic9s4nj3z7sbmcaywoc.libobj?view=1

**2- OpenBrIM Connector** is a package containing COM, .NET and dll libraries providing full access to OpenBrIM capabilities. These include the capabilities of OpenBrIM App, OpenBrIM library. Even the UI components fully exposed and made available to jumpstart your development effort. 

If the objects (class definitions) in your application inherit from OpenBrIM objects (as defined in the Connector library), your project automatically becomes cloud-based and real-time collaboration will work out of the box. OpenBrIM connector allows you to:

* have an object oriented access to OpenBrIM data. 
* embed and integrate OpenBrIM app user interface in your applications. 
* Take advantage of real-time collaboration 
* embed and use standalone UI controls such as 3D graphics window, spreadsheets etc. 

This is the recommended and easiest way to interact with OpenBrIM platform. However it requires some computer resources making it unsuitable for low powered systems. Also supporting COM and .NET interfaces limits the use of the API primarily to Windows desktop (and laptop) computers. While Linux and Mac is also supported (via Mono), it has not been fully tested. 

**3- OpenBrIM App** Service API extends what you can do with the Direct  Data API. While it allows you to access the full functionality of OpenBrIM, it performs all OpenBrIM related processing on cloud -- keeping your application thin and energy efficient. It is particularly suitable for low powered devices such as augmented reality devices, virtual reality devices, UAVs etc. Through this API, you have access to all the functionality of Direct Data API plus the high level functions such as:

* Rendering graphics offsite and on cloud. For example, all processing would be done on OpenBrIM cloud; you would simply get points and triangles representing 3D view of the project for final rendering.
* Perform analysis on cloud and you have acces to analysis results. 
* Perform verifications and design code checks on cloud. 
* and more…

This API allow you to offload all OpenBrIM functionality to OpenBrIM servers keeping your application as resource free as possible. 

We only recommend using this API if you have a definite need for it. Keep in mind that OpenBrIM is an open and  free service. App Service API puts significant more load on our cloud servers than the other two methods. That being said, we recognize that there are some use cases for it. For example, developing for Google Glass or Microsoft HoloLens would require you to use this API, because the computing resources are extremely limited on these devices. 

This document will focus on (2) OpenBrIM Connector as we expect most applications to interact with OpenBrIM through this API. This also gives you full access to UI allowing quick prototyping.

## OpenBrIM System Overview ##

It is important to understand the basic components of the OpenBrIM system before jumping into developing an application for it. If you are new to OpenBrIM system, make sure that you watch the short introduction videos on YouTube. 

Introduction: https://youtu.be/B6tXSv3BrSE
Platform Overview: https://youtu.be/bjANK93gEgQ

If you have not yet looked at the OpenBrIM apps, logon to openbrim.org and explore both the OpenBrIM App and the Library. 

There are 3 types of users on OpenBrIM platform:

* 1) Authors are those who create re-usable, customizable (parametric) information on OpenBrIM platform. Authors use OpenBrIM Library App to create library objects and use ParamML to represent information on OpenBrIM platform. See OpenBrIM Author’s Guide for more information. 
* 2) Developers are those who either:
create original software applications that makes use of the OpenBrIM platform features and consumes OpenBrIM library objects. 
integrate OpenBrIM platform to existing software applications by developing plugins, add-ons etc. 

Developers use a programming language such as C, C++, C#, VB.NET, FORTRAN, JSCRIPT, JAVA etc to develop applications. They would mainly use OpenBrIM Connector library to interact with OpenBrIM platform. 

* 3) Users are those who use the software packages (hat are connected to OpenBrIM by Developers, and make use of the library objects created by the Authors to create and manage engineering projects. 

The target audience of this document is Developers. 

## Your First OpenBrIM App ##

This document will assume that you are on a Microsoft Windows computer. We will use Visual Studio Community Edition which is freely available from Microsoft. 

Run Visual Studio, from the menu, select File > New > Project and pick Windows Forms Application as the project type under Visual Basic. 
Enter a name for the project and hit OK to create it. 
You will have the default Form1 created in your project. 
From the menu, Tools > NuGet Package Manager > Manage NuGet Packages for Solution. 	
In NuGet Solution window, click Browse tab, search for OpenBrIM and install the latest stable version for your project. You can close the NuGet window. 
Goto the code view of Form 1 and make sure that it looks like this:



```
#!VB.NET

Public Class Form1

    ' the connection to OpenBrIM servers
    Private WithEvents conn As OBrIM.OBrIMConn  ' notice WithEvents, we want to handle message event
    Private ctrl As OBrIM.UIWindow              ' OpenBrIM UI control


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' create a connection to OpenBrIM servers
        conn = New OBrIM.OBrIMConn(OBrIM.UIMode.EmbeddedControl, True)

        ' we will add the OpenBrIM UI to the designated panel on the form upon form loading
        ctrl = conn.GetUserControl()    ' the the UI control
        Me.Controls.Add(ctrl)           ' add the UI control on the form
    End Sub

    Private Sub OpenBrIM_MessageReceiver(eventName As String, args As Object) Handles conn.MessageReceived
        ' this subroutine will be used by OpenBrIM to comunicate with our app

        If eventName = "AppLoaded" Then                 ' OpenBrIM application has been loaded
            conn.AppShow(OBrIM.AppViews.NewProject)     ' let user create a new project
        ElseIf eventName = "ProjectOpened" Then         ' user opened up a project
            conn.AppShow(OBrIM.AppViews.Graphics3D)     ' let's display 3D graphics
        End If

        ' print out all the events we receive on console
        Console.Out.WriteLine(eventName)
    End Sub
End Class
```




Run the application. You should see OpenBrIM loading, and then going to New Project window. If you create a new project, the app will switch to 3D view. 

Congratulations, you just ran your first OpenBrIM App.
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OBrIM;

namespace OBrIMCustomObjModelSample
{
    public class Slab: BrIMObj
    {
        public Slab(OBrIMObj o): base(o) {
            this.Width = 30*12;
            this.Depth = 50*12;
            this.Thickness = 6;
        }
        public double Width { get { return brim.ParamValueAsNumber("Width"); } set { brim.SetParamValueNum("Width", value); } }
        public double Depth { get { return brim.ParamValueAsNumber("Depth"); } set { brim.SetParamValueNum("Depth", value); } }
        public double Thickness { get { return brim.ParamValueAsNumber("Thickness"); } set { brim.SetParamValueNum("Thickness", value); } }
    }
}

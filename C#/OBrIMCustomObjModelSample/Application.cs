﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OBrIM;

namespace OBrIMCustomObjModelSample
{
    public class Application
    {
        public static OBrIMConn conn;
        public static Building building;

        public Application()
        {
            Application.conn = new OBrIM.OBrIMConn(UIMode.SeparateWindow);
            Application.conn.MessageReceived += this.Conn_MessageReceived;
        }

        private void Conn_MessageReceived(string eventName, object args)
        {
            if (eventName == "ProjectCompileComplete")
            {
                var o = Application.conn.ObjectGet();

                if (Application.building == null)
                {
                    Application.building = new Building(o);
                } else
                {
                    Application.building.brim = o;
                }
            }
        }

        
    }
}

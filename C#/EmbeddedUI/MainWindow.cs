﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OBrIM;

namespace EmbeddedUI
{
    public partial class MainWindow : Form
    {
        OBrIMConn OpenBrIM;

        public MainWindow()
        {
            InitializeComponent();
            
            // create a new connection object for OpenBrIM
            OpenBrIM = new OBrIM.OBrIMConn(OBrIM.UIMode.EmbeddedControl, true);
            this.panel1.Controls.Add(OpenBrIM.GetUserControl());
        }
        
        
        private void btnLogin_Click(object sender, EventArgs e)
        {
            // ask brim object to log the user in brim object will display the login screen.
            // if user already logged in, it will not do anything
            OpenBrIM.UserLogin();
        }

        
        private void btnLogout_Click(object sender, EventArgs e)
        {
            // ask brim OpenBrIM to log the user out (if no logged in account, it won't have any effect)
            OpenBrIM.UserLogout();
        }

        private void btnOpenProject_Click(object sender, EventArgs e)
        {
            // display open project view
            OpenBrIM.AppShow(AppViews.OpenProject);
        }

        private void btnDisplay3D_Click(object sender, EventArgs e)
        {
            // display 3D graphics view
            OpenBrIM.AppShow(AppViews.Graphics3D);
        }

        private void btnDebug_Click(object sender, EventArgs e)
        {
            // display console debug view
            OpenBrIM.AppShow(AppViews.Console);
        }
    }
}
